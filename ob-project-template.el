;;; ob-project-template.el --- Generate projects from template files -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Oliver Marks

;; Author: Oliver Marks <oly@digitaloctave.com>
;; URL: https://gitlab.com/olymk2/emacs-ob-project-templates
;; Keywords: templates tools
;; Version: 0.1
;; Created 25 July 2018
;; Package-Requires: ((emacs "24.4"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implid warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a small library that lets you define an org file to create new project using org babel
;; You create babel blocks with tangle file names and then run the command to select a folder
;; to deploy the code blocks to the specified project folder

;;; Code:
(require 'org)

(defcustom ob-project-template-project-folder "~/projects"
  "Change settings folder."
  :group 'ob-project-template
  :type '(string))


(defcustom ob-project-template-folder "~/projects/templates/"
  "Change settings folder."
  :group 'ob-project-template
  :type '(string))


(defcustom ob-project-template-left-str "<<"
  "Change settings folder."
  :group 'ob-project-template
  :type '(string))


(defcustom ob-project-template-right-str ">>"
  "Change settings folder."
  :group 'ob-project-template
  :type '(string))


(defun ob-project-template-change-tangle-path (path)
  "Return a modified PATH, inserting substitutions & create missing folders."
  (let ((new-path (ob-project-template-make-substitutions-string path)))
    (let ((folder ( ob-project-template-make-substitutions-string (file-name-directory path))))
      (unless (file-exists-p folder)
        (make-directory folder t)) new-path)))


(defun ob-project-template-fetch ()
  "Return a list of template filenames."
  (if (file-directory-p ob-project-template-folder)
      (mapcar (lambda (filename)
                (file-name-sans-extension (file-name-nondirectory filename)))
              (directory-files ob-project-template-folder t ".org$"))
    (message "Template folder %s does not exist." ob-project-template-folder)))


;;;###autoload
(defun ob-project-template-open ()
  "View the selected template file."
  (interactive)
  (let ((template (ido-completing-read "Select template:" (ob-project-template-fetch))))
    (find-file (concat ob-project-template-folder template ".org"))))


;;;###autoload
(defun ob-project-template-deploy
    (&optional
     override)
  "Deploy template to folder with if OVERRIDE is set allow defaults to be changed."
  (interactive)
  (let ((project-dir (read-directory-name "Select directory:" ob-project-template-project-folder))
        (template (ido-completing-read "Select template:" (ob-project-template-fetch)))
        (override (or override
                      nil)))
    (unless (file-exists-p project-dir)
      (make-directory project-dir))
    (ob-project-template-tangle-single-block project-dir template nil override)))


;;;###autoload
(defun ob-project-template-deploy-override ()
  "Deploy a single file to the selected folder."
  (interactive)
  (ob-project-template-deploy t))


;;;###autoload
(defun ob-project-template-deploy-single-file-override ()
  "Deploy a single file to the selected folder."
  (interactive)
  (ob-project-template-deploy-single-file t))


;;;###autoload
(defun ob-project-template-deploy-single-file
    (&optional
     override)
  "Select PROJECT-DIR and TEMPLATE to deploy files ask user for replacements if OVERRIDE is set."
  (interactive)
  (let ((project-dir (read-directory-name "Select directory:" ob-project-template-project-folder))
        (template (ido-completing-read "Select template:" (ob-project-template-fetch))))
    (let ((template-file (ido-completing-read "Select file:"
                                              (ob-project-template-fetch-tangle-blocks-list
                                               template)))
          (override (or override
                        nil)))
      (ob-project-template-tangle-single-block project-dir template template-file override))))


(defun ob-project-template-fetch-tangle-blocks-list (template)
  "Fetch list of tangle filename from TEMPLATE."
  (let ((tangle-list ()))
    (with-temp-buffer (insert-file-contents (concat ob-project-template-folder template ".org"))
                      (while (re-search-forward "\\#\\+BEGIN_SRC" nil t)
                        (push  (assoc :tangle (nth 2 (org-babel-get-src-block-info 'light)))
                               tangle-list)))
    (mapcar 'cdr (cl-delete-if (lambda (x)
                                 (member (cdr x)
                                         '("yes" "no"))) tangle-list))))


(defun  ob-project-template-tangle-single-block (directory template template-file &optional user)
  "Deploy TEMPLATE to DIRECTORY ouputing TEMPLATE-FILE if set allow USER to override the defaults if set."
  (let ((template-path (concat ob-project-template-folder template ".org"))
        (tangle-list (if template-file (list template-file)
                       (ob-project-template-fetch-tangle-blocks-list template))))
    (with-temp-buffer template-path (insert-file-contents template-path)
                      (ob-project-template-fetch-replacement-list user)
                      (while (re-search-forward "\\#\\+BEGIN_SRC" nil t)
                        (let ((current (cdr (assoc :tangle (nth 2 (org-babel-get-src-block-info
                                                                   'light))))))
                          (if (member current tangle-list)
                              (let ((code (first (cdr (org-babel-get-src-block-info 'light))))
                                    (deploy-path (concat directory current)))
                                (let  ((template-file-path (ob-project-template-change-tangle-path
                                                            deploy-path)))
                                  (with-temp-file template-file-path (insert code)
                                                  (ob-project-template-make-substitutions-current-buffer))))))))))


(defun ob-project-template-make-substitutions-string(text)
  "TODO Make the substitutions in TEXT string."
  (with-temp-buffer (insert text)
                    ( ob-project-template-make-substitutions-current-buffer)
                    (buffer-string)))


(defun ob-project-template-make-substitutions-current-buffer()
  "Make the substitutions in the current buffer."
  (mapc (lambda (row)
          (let ((key (car row))
                (value (first (cdr row))))
            (goto-char (point-min))
            (while (re-search-forward (concat ob-project-template-left-str key
                                              ob-project-template-right-str) nil t)
              (replace-match value t)))) replacements))


(defun ob-project-template-fetch-replacement-list
    (&optional
     user)
  "Get the list of replacements, if USER is set allow changing of the values."
  (interactive)
  (message "user %s" user)
  (goto-char (point-min))
  (re-search-forward org-table-any-line-regexp nil t)
  (if user
      (setq replacements (mapcar (lambda (row)
                                   (list (car row)
                                         (read-string (format "Value for %s:" (car row))
                                                      (first (cdr row)))))
                                 (org-table-to-lisp)))
    (setq replacements (org-table-to-lisp))))


;;; (Features)
(provide 'ob-project-template)
;;; ob-project-template.el ends here
